name = "John"
age = 26
occupation = "movie critique"
movie = "Iron Man"
rating = 95.50

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 5, 10, 15

product = num1*num2

is_less = num1 < num3

add_value = num3 + num2

print(product)
print(is_less)
print(add_value)
