# [Section] Comments
# Comments in Python are done using "ctrl + /" or #


# [Section] Python Syntax
# Hello World in Python

print("Hello World")

# [Section] Indentation
# Where in other programming language the indentation in code is for readbility only, but the indentation in Python is very important.
# In Python, indentation is used to indicate a block of code.
# Similar to JS, there is no need to end statements with semi-colons

# [Section] Variables
# Variables are containers of data
# In Python, a variable is declare by stating the variable name and assigning a value using the equality symbol


# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers shoud begin with a letter (A to Z or a to z). dollar sign or a underscore.
# After the first character, indentifier can have any combination of characters.
# Unlike JavaScript the uses camelCasing, Python uses snake_casing convention for variables as defined in the PEP (Python Enhancement Proposal).
age = 35
middle_initial = "C"

# Python allows assigning of values to multiple variables in one line.

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name4)

# [Section] Data Types what kind of information a variable holds. There are different data types and each has its own use.

# Ub python, there are the commonly used data types:

# 1. Strings(str) - for alphanumeric and symbols.

full_name = "John Doe"
secret_code = "Password"

# 2. Numbers(int,float,complex) - for integers, decimal and complex numbers
num_of_days = 365  # This is an integer
pi_approx = 3.1416  # This is a float
complex_num = 1 + 5j  # This is a complex number, letter j represents the imaginary

print(type(full_name))

# 3. Boolean(bool) - for truth values
# Boolean values in Python starts with uppercase letters
isLearning = True
isDifficult = False


# [Section] Using Variables
# Just like in JS variables are used by simply calling the name of the indentifier

print(full_name + " " + secret_code)

# [Section] Terminal Outputs
# In python, printing in the terminal uses the print() function
# To use variable, concatenate (+symbol) between strings can be used

# print("My age is" + age)

# [Section] Typecasting
# Here are some functions that can be used in typecasting

# 1. int() - convert the value into an integer
print(int(3.15))

# 2. float() - converts the value into an integer value
print(float(5))

# 3. str() - converts the value into string
print("My age is " + str(age))

# Another way to avoid type error in printing without the use of typecasting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}.")


# [Section] Operations
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18 % 4)
print(2**6)

# Assignment operators - used to assign to variables

num1 = 4

num1 += 3

print(num1)


# other operators -=, *=, /=, %=

# Comparison operators - used compare values (boolean values)

print(1 == "1")
# other operators, !=, >=, <=, >, <

# Logical operators - used to combine conditional statements
# Logical Or Operator
print(True or False)
# Logical And Operator
print(True and False)
# Logical Not Operator
print(not False)
